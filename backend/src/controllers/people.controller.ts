import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParam,
} from "routing-controllers";
import { PeopleProcessing } from "../services/people_processing.service";

const peopleProcessing = new PeopleProcessing();

// url?page=3&limit=4

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get("/all")
  getAllPeople(
    @QueryParam("page") page: number,
    @QueryParam("limit") limit: number
  ) {
    const people = peopleProcessing.getAll();

    if (!people) {
      throw new NotFoundError("No people found");
    }

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const result = people.slice(startIndex, endIndex);

    return {
      data: result,
    };
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }
}
